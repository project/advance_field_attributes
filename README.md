# Advance Field Attributes

This module allows you to to configure and add extra advance attributes to 
Fields Like text (plain, formatted, long, List), Email Field, Entity Reference 
field, Number Field(Integer, float, decimal, List Integer, List Float).

For a full description of the module, visit the
[Project Page](https://www.drupal.org/project/advance_field_attributes).

Submit bug reports and feature suggestions, or track changes in the
[Issue Queue](https://www.drupal.org/project/issues/advance_field_attributes).


## Requirements

This module requires no modules outside of Drupal core.


## Installation

If your site is managed via Composer, use Composer to download the module.

1. `composer require drupal/advance_field_attributes`
1. `drush en advance_field_attributes`
1. `drush cr`

Or

Install as you would normally install a contributed Drupal module. For further
information, see
[Installing Drupal Modules](https://www.drupal.org/docs/extending-drupal/installing-drupal-modules).


## Configuration

Enable the module at Administration > Extend.


## Usage

The field formatter attributes settings are found in the Manage display tab for
content types, users, and other entities.
A options element is available for each field formatter, revealed by using 
the formatter settings edit button (Gear wheel icon) for that field.